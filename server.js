var express = require('express'),
	app = express(),
	engines = require('consolidate');
var server = require('http').Server(app);

var io = require('socket.io')(server);

server.listen('3000', function(){
	console.log("chat server started on port 3000 ...");
});

app.engine('hbs', engines.handlebars);

app.get('/', function(req, res){
	res.render('index.hbs');
});

app.use(express.static(__dirname + '/bower_components'));
app.use(express.static(__dirname + '/public'));

io.on('connection', function(socket){
	socket.on('userMessage', function(username, message){
		io.sockets.emit('message', {
			username: username,
			message: message
		});
	});
});