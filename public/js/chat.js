var socket = io.connect('http://' + window.location.hostname);

var username='';

$(document).ready(function() {

	$('#chatMessageForm').submit(function(event) {
		event.preventDefault();
		var userMessage = $('#chatMessage').val();

		if (username == '') { 
			showError('Please choose a username');
		} else if (userMessage != '') { 
			console.log('chat message btn');
			socket.emit('userMessage', username, userMessage);
			$('#chatMessage').val("");
		}
	});

	$('#chatUsernameForm').submit(function(event) {
		event.preventDefault();
		username = $('#username').val();
		hideUsernameFormOrShowError(username);
	});

});

function showError(message) {
	$('#alertMessageText').text(message);
	$('#alertMessage').fadeIn();
}

function hideUsernameFormOrShowError(text) {
	if ($.trim(text)!='') {
		username=$.trim(text);
		$("#chatUsernameForm").fadeOut();
	} else {
		showError("Username should not be blank")
	}
}

socket.on('message', function(data) {
	var usernameFormatted = '<font color="red">' + data['username'] + ':</font>'
	$('#chatMessageBoard').append(usernameFormatted + ' ' + data['message'] + '<br />'); 
});